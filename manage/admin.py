from django.contrib import admin
from .models import Request,RequestStatus
from UserManagement.models import C24Team,C24UserTeamMap
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import Group
from django.contrib.auth.models import User as AuthUser
from django.db.models import signals
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_control
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import SimpleListFilter
from django.forms import ModelChoiceField

class CustomModelChoiceField(ModelChoiceField):
	def label_from_instance(self,obj):
		return "%s" % (obj.auth_user)

class AssignedForm(ModelForm):
	assigned_to = CustomModelChoiceField(queryset=C24UserTeamMap.objects.all())
	class Meta:
		model = Request
		fields = ('assigned_to',)

	def __init__(self, *args, **kwargs):
		inst = kwargs.get('instance')
		super(AssignedForm, self).__init__(*args, **kwargs)
		if inst:
			self.fields['assigned_to'].queryset = C24UserTeamMap.objects.filter(c24team=inst.c24team)


class RequestInlineAdmin(admin.TabularInline):
	model = RequestStatus
	extra =0
	ordering =('-tstamp', )
	readonly_fields = ('statuscheck','tstamp','changes')
	fieldsets = (
		(None, {
			"fields": readonly_fields
		}),
	)
	verbose_name = 'OldStatus'
	verbose_name_plural = 'OldStatuses'

	def has_delete_permission(self,request,obj=None):
		return False
	def has_add_permission(self, request):
		return False

class AssignedListFilter(admin.SimpleListFilter):
	title = _('assigned_to')
	parameter_name = 'assigned'
	def lookups(self,request,RequestTypeAdmin):
		assignedlist = []
		for r in RequestTypeAdmin.model.objects.all():
			if(r.assigned_to != None):
				if r.assigned_to in assignedlist:
					continue
				else:
					assignedlist.append(r.assigned_to)
		return [(r.id,r.auth_user)for r in assignedlist]
	def queryset(self,request,queryset):
		if self.value():
			return queryset.filter(assigned_to__id__exact=self.value())
		else:
			return queryset

class ApprovalListFilter(admin.SimpleListFilter):
	title = _('approval_by')
	parameter_name = 'approved_by'
	def lookups(self,request,RequestTypeAdmin):
		approvallist = []
		for r in RequestTypeAdmin.model.objects.all():
			if r.approval_by != None:
				if r.approval_by in approvallist:
					continue
				else:
					approvallist.append(r.approval_by)
		return [(r.id,r.auth_user)for r in approvallist]
	def queryset(self,request,queryset):
		if self.value():
			return queryset.filter(approval_by__id__exact=self.value())
		else:
			return queryset


class RequestTypeAdmin(admin.ModelAdmin):
	show_full_result_count = False
	list_display = ('id','c24team','request_type','priority','sample','approval_flag','status','created','modified','upper_case_name','lower_case_name')
	list_filter = ('c24team','priority','request_type','status',ApprovalListFilter,AssignedListFilter,'approval_flag',)
	# exclude = ('user',)
	search_fields = ['=id','=c24team__name','=assigned_to__auth_user__username']

	def upper_case_name(self,obj):
		if obj.assigned_to != None:
			return ("%s" % (obj.assigned_to.auth_user.username)).upper()
		else:
			return "None"
	upper_case_name.short_description = 'Assigned To'

	def lower_case_name(self,obj):
		if obj.approval_by != None:
			return ("%s" % (obj.approval_by.auth_user.username)).upper()
		else:
			return "None"
	lower_case_name.short_description = 'Approved By'
	
	def get_form(self, request, obj, **kwargs):
		new_userprofile = C24UserTeamMap.objects.get(auth_user=request.user)
		if new_userprofile.team_role == 2:
			if obj:
				if (obj.approval_by == None and obj.c24team == new_userprofile.c24team ) or obj.approval_by == new_userprofile:
					kwargs['form'] = AssignedForm
		return super(RequestTypeAdmin, self).get_form(request, obj, **kwargs)
		# 	else:
		# 		return super(RequestTypeAdmin, self).get_form(request, obj, **kwargs)
		# else:
		# 	return super(RequestTypeAdmin, self).get_form(request, obj, **kwargs)
			# else
			# 	return super(RequestTypeAdmin, self).get_form(request, obj, **kwargs)


	def get_fieldsets(self,request,obj):
		if obj:
			fieldsets = ((None,{'fields':('c24team','request_type','priority','description','sample','approval_flag','approval_by','assigned_to','status')}),)
		else:
			fieldsets = ((None,{'fields':('c24team','request_type','priority','description','sample')}),)
		
		return fieldsets
		#new_userprofile = C24UserTeamMap.objects.get(auth_user=request.user)
		#if new_userprofile.team_role == 'Team Lead':
		# else:
		# 	if obj:
		# 		fieldsets = ((None,{'fields':('c24team','request_type','priority','description','sample','approval_flag','approval_by','assigned_to','status')}),)
		# 	else:
		# 		fieldsets = ((None,{'fields':('c24team','request_type','priority','description','sample')}),)

				# obj.save(user=request.user.username)
				# obj.approval_flag = True
				# obj.approval_by = request.user

	# def get_my_choices(self,request,obj):
	# 	choices = User.objects.filter(groups_name = obj.c24team)
	# 	return choices
	# def formfield_for_foreignkey(self, db_field, request, **kwargs):
	# 	if db_field.name == "assigned_to":
	# 		kwargs["queryset"] = C24UserTeamMap.objects.filter(c24team= 'c24team')
	# 	return super(RequestTypeAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
	
	def save_model(self, request, obj, form,changed):
		# when lead is being created... remarks necessary ... stat can be loaded
		# if changed: 
		# 	old_obj = Request.objects.get(pk=obj.pk)
		# if obj:
		# 	obj.assigned_to.choices = self.get_my_choices(request,obj)
		# 	obj.approval_by.choices = self.get_my_choices(request,obj)
		new_userprofile = C24UserTeamMap.objects.get(auth_user=request.user)
		if new_userprofile.team_role == 2:
			if obj:
				# obj.assigned_to.limit_choices_to = {'group_name': obj.c24team}
				# obj.approval_by.limit_choices_to = {'group_name': obj.c24team,'is_superuser':True}
				if obj.approval_flag and obj.approval_by == None:
					obj.approval_by = new_userprofile
				if  obj.approval_flag != True:
					obj.approval_by = None
					obj.assigned_to = None
			# else:
			# 	obj.approval_flag = True
			# 	obj.approval_by = request.user

		obj.save(user=request.user.username)
		# obj.checkapproval(user=request.user.username)
	
	
	def has_delete_permission(self,request,obj=None):
		new_userprofile = C24UserTeamMap.objects.get(auth_user=request.user)
		if new_userprofile.team_role != 2:
			return False
		if obj:
			if obj.approval_by == new_userprofile:
				return True
			elif obj.approval_by == None and obj.c24team == new_userprofile.c24team:
				return True
			else:
				return False
		# else:
		# 	return False
	

	def get_readonly_fields(self,request,obj):
		new_userprofile = C24UserTeamMap.objects.get(auth_user=request.user)
		if obj:
			if new_userprofile.team_role != 2:
				if obj.assigned_to == new_userprofile:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_by','assigned_to','approval_flag')
				else:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_by','assigned_to','approval_flag','status')
			else:
				if obj.approval_by == new_userprofile and obj.assigned_to == new_userprofile:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_by')
				elif obj.assigned_to == new_userprofile and obj.approval_by != new_userprofile:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_by','approval_flag','assigned_to')
				elif obj.approval_by == new_userprofile and obj.assigned_to != new_userprofile:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_by','status')
				elif obj.approval_by == None and new_userprofile.c24team == obj.c24team:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_by','status')
				else:
					return self.readonly_fields+('c24team','request_type','priority','description','sample','approval_flag','approval_by','assigned_to','status')
		return self.readonly_fields
	inlines= [RequestInlineAdmin,]

# # @receiver(post_save,sender=RequestTypeAdmin)
# # def approval_check(sender,instance,**kwargs):
# # 	if request.user.is_superuser:
# # 		new_obj = Request.objects.get(id = obj.id)
# # 		if new_obj.assigned_to != None:
# # 			print request.user.username
# # 			if obj.approval_by == None:
# # 				obj.approval_flag = True
# # 				obj.approval_by = request.user
# # 	obj.save(user=request.user.username)
# # 	signals.post_save.connect(update_status, sender=RequestTypeAdmin)
# # @receiver(pre_delete, sender=Request)
# # def delete_request(sender, instance, **kwargs):
# # 	if instance.approval_by != request.user:
# # 		raise PermissionDenied


class RequestStatusAdmin(admin.ModelAdmin):
	list_display = ('id','request_made','tstamp','statuscheck')
	list_filter = ('request_made','statuscheck',)
	def has_add_permission(self, request):
		return False
	def has_delete_permission(self,request,obj=None):
		return False
	def get_readonly_fields(self,request,obj=None):
		return self.readonly_fields+('request_made','statuscheck','changes')

# @receiver(pre_save,sender = RequestTypeAdmin)
# def checkapproval(sender,instance,**kwargs):
#   New_Request = RequestTypeAdmin.objects.get(id = instance.id)
#   if New_Request.approval_flag: 
#     instance.approval_by = request.user
#     instance.save_model(request,obj,form,changed)
# def has_change_permission(self, request, obj=None):
	# def restrictfields(self,request,obj=None):
	# 	if request.user == obj.assigned_to:
	# 		readonly_fields = ('request_type','priority','description','sample',)
	# 	return readonly_fields

# Register your models here.
# class NormalUser(admin.ModelAdmin):
# 	list_display = ('user','c24team')
# 	list_filter = ('c24team',)
# 	search_fields = ('user__username',)
# 	def has_delete_permission(self,request,obj=None):
# 		if obj:
# 			if request.user.is_superuser:
# 				return True
# 			else:
# 				return False
# 		else:
# 			return False

# 	def has_add_permission(self,request,obj=None):
# 		if request.user.is_superuser:
# 			return True
# 		else:
# 			return False
		



# class Headofc24team(admin.ModelAdmin):
# 	list_display = ('superuser','c24team')
# 	list_filter = ('c24team',)
# 	search_fields = ('superuser__username',)
# 	def has_delete_permission(self,request,obj=None):
# 		if obj:
# 			if request.user.is_superuser:
# 				return True
# 			else:
# 				return False
# 		else:
# 			return False

# 	def has_add_permission(self,request,obj=None):
# 		if request.user.is_superuser:
# 			return True
# 		else:
# 			return False
		

# class PartofCompany(admin.ModelAdmin):
# 	def has_delete_permission(self,request,obj=None):
# 		if obj:
# 			if request.user.is_superuser:
# 				return True
# 			else:
# 				return False
# 		else:
# 			return False

# 	def has_add_permission(self,request,obj=None):
# 		if request.user.is_superuser:
# 			return True
# 		else:
# 			return False 

admin.site.register(Request,RequestTypeAdmin)
admin.site.register(RequestStatus,RequestStatusAdmin)


