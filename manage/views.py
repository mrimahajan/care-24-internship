from django.shortcuts import render, get_object_or_404,redirect
import datetime
from django.views import generic
from .models import Request,RequestStatus
from django.contrib.auth import authenticate, login
from .forms import RequestForm,ApprovalForm,ChangeStatus
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.views import password_reset,password_reset_confirm
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
@csrf_exempt
def loggin(request):
	return render(request,'manage/loggin.html',{})

@csrf_exempt
def auth(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')

	user = authenticate(username=username, password=password)
	state =''
	if user is not None:
		if user.is_active:
			login(request,user)
			return redirect('/auth/loggedin')
		else:
			state = 'Please contact admin your acct in inactive'
			return render(request,'manage/invalid.html',{'state': state})
	else:
		state = "Your username and/or password were incorrect."
		return render(request,'manage/invalid.html',{'state': state})
				
		
@csrf_exempt
def loggedin(request):
	form = RequestForm()
	
	if request.POST:
		form = RequestForm(request.POST)
		if form.is_valid():
			req = form.save(commit=False)
			req.user = request.user
			req.save()
	reqapproved=Request.objects.filter(user=request.user).filter(approval_flag=True)
	reqpending= Request.objects.filter(user=request.user).filter(approval_flag=False)
	reqassg=Request.objects.filter(assigned_to=request.user)
	return render(request,'manage/loggedin.html',{'form': form,'reqapproved': reqapproved,'reqpending': reqpending,'reqassg': reqassg})

			 
def logout(request):
	return render(request,'manage/logout.html')

@csrf_exempt
def stafflogin(request):
	return render(request,'manage/stafflogin.html',{})

@csrf_exempt
def authstaff(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')

	user = authenticate(username=username, password=password)
	state =''
	if user is not None:
		if user.is_active:
			if user.is_staff:
				login(request,user)
				return redirect('/staffloggin/authstaff/staffloggedin')
			else:
				state = 'You are not a staff member.Permission Denied'
				return render(request,'manage/staffinvalid.html',{'state': state})
		else:
			state = 'Please contact admin your acct in inactive'
			return render(request,'manage/staffinvalid.html',{'state': state})
	else:
		state = "Your username and/or password were incorrect."
		return render(request,'manage/staffinvalid.html',{'state': state})

@csrf_exempt
def staffloggedin(request):
	reqlist = Request.objects.filter(approval_flag=False)
	return render(request,'manage/staffloggedin.html',{'reqlist': reqlist})


@csrf_exempt
def req_detail(request,pk):
	req = get_object_or_404(Request, pk=pk)
	form = ApprovalForm(request.POST or None)
	if request.POST and form.is_valid():
			req.approval_flag = form.cleaned_data['approval_flag']
			if req.approval_flag:
				req.approval_by = request.user
				req.assigned_to = form.cleaned_data['assigned_to']
			req.save()
	return render(request,'manage/requestdetail.html',{'req':req,'form': form})
		
@csrf_exempt
def stafflogout(request):
	return render(request,'manage/stafflogout.html')
		
@csrf_exempt
def displayrequest(request,pk):
	req = get_object_or_404(Request, pk=pk)
	return render(request,'manage/displayrequest.html',{'req':req})

@csrf_exempt
def changestatus(request,pk):
	req = get_object_or_404(Request, pk=pk)
	form = ChangeStatus(request.POST or None)
	if request.POST and form.is_valid():
			req.status = form.cleaned_data['status']
			orig = Request.objects.get(pk=req.pk)
			if orig.status != req.status:
				req.save()
				requeststatus = RequestStatus()
				requeststatus.create(req)
	return render(request,'manage/changestatus.html',{'req':req,'form': form})
# @csrf_exempt
# def doinquiry(request):
# 	form = InquiryForm()
	
# 	if request.POST:
# 		form = InquiryForm(request.POST)
# 		if form.is_valid():
# 			inquiry = form.save(commit=False)
# 			inquiry.user = request.user
# 			inquiry.save()
			
# 	return render(request,'manage/inquiry.html',{'form': form})
# @csrf_exempt
# def getdatetime(request,pk):
# 	inquiry = get_object_or_404(Inquiry, pk=pk)
# 	Requeststatus_required= {}
# 	try:
# 		Requeststatus_required = RequestStatus.objects.filter(request = inquiry.request).get(statuscheck = inquiry.status_required)
# 		result=str(Requeststatus_required.Datecheck)
# 	except ObjectDoesNotExist:
# 		result = "Sorry,this request hasn't yet reached the status you enquired"		
	
# 	return render(request,'manage/result.html',{'result': result})


# def my_password_reset(request, template_name='path/to/my/template'):
#     return password_reset(request, template_name)

# def reset(request):
#     # Wrap the built-in password reset view and pass it the arguments
#     # like the template name, email template name, subject template name
#     # and the url to redirect after the password reset is initiated.
#     return password_reset(request, template_name='reset.html',
#         email_template_name='reset_email.html',
#         subject_template_name='reset_subject.txt',
#         post_reset_redirect=reverse('success'))