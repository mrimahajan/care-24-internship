from django import forms
from .models import Request,RequestStatus
class RequestForm(forms.ModelForm):
      class Meta:
           model = Request
           fields = ('request_type','priority','sample','description','status')

class ApprovalForm(forms.ModelForm):
	class Meta:
		model = Request
		fields = ('approval_flag','assigned_to')

class ChangeStatus(forms.ModelForm):
	class Meta:
		model = Request
		fields = ('status',)


# class InquiryForm(forms.ModelForm):
#     class Meta:
#     	model = Inquiry
#     	fields =('request','status_required')


	