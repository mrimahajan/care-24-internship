from django.conf.urls import url
from django.contrib.auth.views import password_reset,password_reset_confirm,logout
from django.core.urlresolvers import reverse
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import password_reset

urlpatterns = [
    url(r'^$', views.loggin, name='loggin'),
    url(r'^auth/loggedin/$',views.loggedin, name='loggedin'),
    # url(r'^auth/loggedin/change-password/$', views.reset, name="password-change"),
    # url(r'^change-password-done/$', 'django.contrib.auth.views.password_change_done', {'template_name': 'password_change_done.html'}, name="password-change-done"),
    url(r'^/auth/loggedin/resetpassword/passwordsent/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^/auth/loggedin/resetpassword/$', 'django.contrib.auth.views.password_reset', name ='password-change'),
    url(r'^/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm'),
    url(r'^/reset/done/$', 'django.contrib.auth.views.password_reset_complete'),
   #url(r'^auth/loggedin/doinquiry/$',views.doinquiry, name='doinquiry'),
   #url(r'^auth/loggedin/request/inquiry/(?P<pk>\d+)/$',views.getdatetime, name='getdatetime'),
    url(r'^auth/loggedin/logout/$',views.logout,name='logout'),
    url(r'^auth/$',views.auth, name='auth'),
    url(r'^auth/loggedin/request/(?P<pk>\d+)/$',views.displayrequest, name='displayrequest'),
    url(r'^auth/loggedin/request/changestatus/(?P<pk>\d+)/$',views.changestatus, name='changestatus'),
    url(r'^staffloggin/$',views.stafflogin, name='stafflogin'),
    url(r'^staffloggin/authstaff/$',views.authstaff, name='authstaff'),
    url(r'^staffloggin/authstaff/staffloggedin/$',views.staffloggedin, name='staffloggedin'),
    url(r'^staffloggin/authstaff/staffloggedin/request/(?P<pk>\d+)/$',views.req_detail,name='req_detail'),
    url(r'^staffloggin/authstaff/staffloggedin/stafflogout/$',views.stafflogout,name='stafflogout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

