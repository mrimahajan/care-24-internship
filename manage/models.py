from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User,Group
from datetime import datetime,timedelta
from django import forms
from django.contrib.auth.backends import ModelBackend
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from time import time
from django.core.files.storage import Storage
from django.db.models.signals import post_save
from django.dispatch import receiver
from C24Utils.ModelTools import hconfig, history
from django.db.models import signals
# from smart_selects.db_fields import ChainedForeignKey
# from smart_selects.db_fields import ChainedManyToManyField
# from smart_selects.db_fields import GroupedForeignKey
# from UserManagement.models import C24Team
# from crum import get_current_user
# Create your models here
typechoices = (("DATA",'Data'),("ERROR",'Error'),("NEW_FEATURE",'new feature'),("CHANGE",'change'))
statuschoices =(('open',"OPEN"),('wip',"WIP"),('closed',"CLOSED"),('pending',"PENDING"))
prioritychoices=(('high',"High"),('med','MED'),('low','LOW'))
# departmentchoices = (('Technical',"Technical"),('Allocation',"Allocation"),('Operations',"Operations"),('Marketing',"Marketing"),('HR',"HR"))
fs = FileSystemStorage(location='media')
def get_upload_file_name(instance,filename):
  return "manage/static/manage/%s_%s"%(str(time()).replace('.','_'),filename.replace(' ','_')) 
# for key in departmentchoices:
#   required_user[key] = User.objects.filter(groups_name = key)r
# class Department(models.Model):
#   department_name = models.CharField(max_length=20)

  # def __str__(self):
  #   return self.department_name

# class UserProfile(models.Model):
#   user = models.OneToOneField(User)
#   c24team = models.ForeignKey("UserManagement.C24Team")

#   def __str__(self):
#     return self.user.username

# class SuperUser(models.Model):
#   superuser = models.OneToOneField(User,limit_choices_to={'is_superuser':True})
#   c24team = models.ForeignKey("UserManagement.C24Team")

#   def __str__(self):
#           return self.superuser.username

class Request(models.Model):
      # user = models.ForeignKey(User,null=True,related_name="user")
      c24team = models.ForeignKey("UserManagement.C24Team")
      request_type = models.CharField(max_length=20,choices=typechoices)
      priority=models.CharField(max_length=20,choices=prioritychoices)
      description = models.TextField()
      sample = models.FileField(storage=fs,blank=True,null=True)
      approval_flag = models.BooleanField(default=False)
      status = models.CharField(max_length=20,choices=statuschoices,default='pending')
      created=models.DateTimeField(auto_now_add=True,null=True)
      modified = models.DateTimeField(auto_now=True)
      assigned_to = models.ForeignKey("UserManagement.C24UserTeamMap",null=True,blank=True,related_name='assigned')
      approval_by = models.ForeignKey("UserManagement.C24UserTeamMap",null=True,blank=True,related_name='approval',limit_choices_to={'team_role':2})

      def __str__(self):
          return str(self.id)

      # def get_my_choices(self):
      #   choices = User.objects.filter(groups_name = self.department)
      #   return choices

      def save(self,*args,**kwargs):
        user = 'SYSTEM'
        if 'user' in kwargs:
          user = kwargs.pop('user')
        # user = get_current_user()
        # new_user = User(id = user.id)
        # self.assigned_to = new_user
        # self.approval_by = new_user
        super(Request, self).save(*args, **kwargs)
        history.add_history_obj(self, hconfig.get_config(self._meta.app_label, self._meta.model_name, RequestStatus),user)

      # def checkapproval(self,*args,**kwargs):
      #   user = 'SYSTEM'
      #   if 'user' in kwargs:
      #     user = kwargs.pop('user')
      #   if self.assigned_to != None and self.approval_by == None:
      #     self.approval_flag = True
      #     self.approval_by = user
      #   self.save(user)
      # def __init__(self, *args, **kwargs):
      #   super(Request, self).__init__(*args, **kwargs)
      #   instance = kwargs.get('instance', None)
      #   if instance != None:
      #     self.assigned_to.choices = self.get_my_choices
      #     self.approval_by.choices = self.get_my_choices.filter(is_superuser=True)

# @receiver(post_save,sender = Request)
# def assigntask (sender,instance,**kwargs):
#   instance.assigned_to.limit_choices_to = {'groups_name': instance.department}
#   instance.approval_by.limit_choices_to = {'is_superuser':True,'groups_name':instance.department}
#   signals.post_save.connect(update_status, sender=Request)

#   # user = 'SYSTEM'
#   # if 'user' in kwargs:
#   #   print user.username
#   #   print '123'
#   #   user = kwargs.pop('user')
#   # New_Request = Request.objects.get(id = instance.id)
#   # if New_Request.approval_flag: 
#   #   instance.approval_by = user

# class TaskAssignment(models.Model):
# class AssignTask(models.Model):
#   request_judged = models.ForeignKey("Request")
#   assigned_to = models.ForeignKey(User,null=True,blank=True)
#   approval_by = models.ForeignKey(User,null=True,blank=True)

#   def save(self,request_judged):
#     self.request_judged = request_judged
#     self.approval_by.limit_choices_to = {'is_superuser': True,'groups_name': request_judged.department}


class RequestStatus(models.Model):
      request_made = models.ForeignKey("Request")
      # Datecheck = models.DateTimeField(null=True,blank =True)
      statuscheck = models.CharField(max_length=20)
      changes=models.CharField(null=True,max_length=150)
      tstamp=models.DateTimeField(auto_now_add=True,null=True)

      def save(self,*args,**kwargs):
        user = 'SYSTEM'
        if 'user' in kwargs:
          user = kwargs.pop('user')
        super(RequestStatus, self).save(*args, **kwargs)

      # def save(self,request):
      #   self.request_made = returnequest
      #   self.statuscheck = self.request_made.status
      #   self.Datecheck = self.request_made.modified
        

      def __str__(self):
        return str(self.id)
    