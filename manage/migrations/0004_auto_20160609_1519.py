# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('manage', '0003_auto_20160609_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='approval_by',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field='c24team', related_name='approval', chained_field='c24team', blank=True, to='UserManagement.C24UserTeamMap', null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='assigned_to',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field='c24team', related_name='assigned', chained_field='c24team', blank=True, to='UserManagement.C24UserTeamMap', null=True),
        ),
    ]
