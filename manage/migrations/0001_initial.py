# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('UserManagement', '0029_auto_20160530_1448'),
    ]

    operations = [
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_type', models.CharField(max_length=20, choices=[('DATA', 'Data'), ('ERROR', 'Error'), ('NEW_FEATURE', 'new feature'), ('CHANGE', 'change')])),
                ('priority', models.CharField(max_length=20, choices=[('high', 'High'), ('med', 'MED'), ('low', 'LOW')])),
                ('description', models.TextField()),
                ('sample', models.FileField(storage=django.core.files.storage.FileSystemStorage(location='media'), null=True, upload_to=b'', blank=True)),
                ('approval_flag', models.BooleanField(default=False)),
                ('status', models.CharField(default='pending', max_length=20, choices=[('open', 'OPEN'), ('wip', 'WIP'), ('closed', 'CLOSED'), ('pending', 'PENDING')])),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('approval_by', smart_selects.db_fields.GroupedForeignKey(related_name='approval', group_field='c24team', blank=True, to='UserManagement.C24UserTeamMap', null=True)),
                ('assigned_to', smart_selects.db_fields.GroupedForeignKey(related_name='assigned', group_field='c24team', blank=True, to='UserManagement.C24UserTeamMap', null=True)),
                ('c24team', models.ForeignKey(to='UserManagement.C24Team')),
            ],
        ),
        migrations.CreateModel(
            name='RequestStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('statuscheck', models.CharField(max_length=20)),
                ('changes', models.CharField(max_length=150, null=True)),
                ('tstamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('request_made', models.ForeignKey(to='manage.Request')),
            ],
        ),
    ]
