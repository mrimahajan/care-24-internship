# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('manage', '0002_auto_20160609_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='approval_by',
            field=smart_selects.db_fields.GroupedForeignKey(related_name='approval', group_field='c24team', blank=True, to='UserManagement.C24UserTeamMap', null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='assigned_to',
            field=smart_selects.db_fields.GroupedForeignKey(related_name='assigned', group_field='c24team', blank=True, to='UserManagement.C24UserTeamMap', null=True),
        ),
    ]
