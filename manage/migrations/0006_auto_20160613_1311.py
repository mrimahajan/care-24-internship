# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manage', '0005_auto_20160609_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='approval_by',
            field=models.ForeignKey(related_name='approval', blank=True, to='UserManagement.C24UserTeamMap', null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='assigned_to',
            field=models.ForeignKey(related_name='assigned', blank=True, to='UserManagement.C24UserTeamMap', null=True),
        ),
    ]
