$("#sendsms").on("click",function(e){
$('#smsform').show();
  e.preventDefault();
});
var $smsSelect = $("#sms_template");
$.ajax({
   url: '/imgr/smstemplates',
      success: function(data) {
        var sms = data.templates;
        for (var i=0;i<sms.length;i++) {
          if (i===0)
            $('.sms-template-div').empty().append(sms[i].template_text);
          $smsSelect.append('<option value="' + sms[i].id + '">' + sms[i].name + '</option>');
        }
        $smsSelect.on('change', function(e) {
          for (var i=0;i<sms.length;i++) {
            if (sms[i].id == $smsSelect.val()) {
              $('.sms-template-div').empty()
                  .append(sms[i].template_text);
              break;
            }
          }
           e.preventDefault();
        });
      },  
});
 get_cookie = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  };
$('#l-send-sms').on('click', function(e) {
          var $this = $(this);
          $this.attr('disabled', 'disabled');
          $.ajax({
              url: '/imgr/sendsmstemplate',
              data: $('#sta-form').serialize(),
              method: 'post',
              headers: {'X-CSRFToken': get_cookie('csrftoken')},
              success: function(data) {
                  alert(data);
                  $this.removeAttr('disabled');
              },
              error: function(data) {
                  alert(data.responseText || data.toString());
                  $this.removeAttr('disabled');
              },
      // error: function(data) {
      //   alert("Error retreiving sms templates");
      // },
          });
          e.preventDefault();    
    });